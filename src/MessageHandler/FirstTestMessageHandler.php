<?php

namespace App\MessageHandler;

use App\Message\TestMessage;
use Exception;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class FirstTestMessageHandler implements MessageHandlerInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    public function __invoke(TestMessage $message)
    {        
        $this->logger->info('TestMessage handled in FirstTestMessageHandler');
        throw new Exception('Error thrown in FirstTestMessageHandler');
    }
}