<?php

namespace App\MessageHandler;

use App\Message\TestMessage;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class SecondTestMessageHandler implements MessageHandlerInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    public function __invoke(TestMessage $message)
    {
        $this->logger->info('TestMessage handled in SecondTestMessageHandler');
    }
}